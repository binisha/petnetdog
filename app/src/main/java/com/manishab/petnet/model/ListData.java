package com.manishab.petnet.model;

public class ListData {
    public String dogName;
    public int dogImage;

    public ListData(String dogName, int dogImage) {
        this.dogImage = dogImage;
        this.dogName = dogName;
    }
}
