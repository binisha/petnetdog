package com.manishab.petnet;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

public class SplashActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);

//        final ImageView iv = (ImageView) findViewById(R.id.imageView);
//        iv.startAnimation(an);
//        an.setAnimationListener(new Animation.AnimationListener() {
//
//            @Override
//
//            public void onAnimationStart(Animation animation) {
//
//            }
//
//            @Override
//            public void onAnimationEnd(Animation animation) {
//                iv.startAnimation(an2);
//                finish();
//                Intent i = new Intent(getBaseContext(),MainActivity.class);
//                startActivity(i);
//            }
//
//            @Override
//            public void onAnimationRepeat(Animation animation) {
//
//            }
//        });

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent i = new Intent(getBaseContext(),MainActivity.class);
                startActivity(i);
                finish();
            }
        }, 2000);

    }

}

