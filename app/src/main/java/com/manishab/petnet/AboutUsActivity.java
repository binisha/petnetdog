package com.manishab.petnet;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

/**
 * Created by priyanka on 3/5/17.
 */

public class AboutUsActivity extends AppCompatActivity {
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("About Us");
        setSupportActionBar(toolbar);
        TextView textView;
        textView = (TextView) findViewById(R.id.About);
        textView.setText("Dogs club is an online community for compassionate people who love dogs. It helps you make intelligent decisions to keep your dog healthy and happy. You can also find mapped information about dog centers that can help rehabilitate a stray dog in poor conditions. Dogs make world a better place to live; you can make theirs!");
    }
}